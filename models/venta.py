# -*- coding: utf-8 -*-
from database import Database


class Venta:
    def __init__(self, venta):
        self._estado = venta.get('estado', False)
        self._producto_id = venta.get('producto_id', False)
        self._measure_id = venta.get('measure_id', False)
        self._cantidad = venta.get('cantidad', False)
        self._precio = venta.get('precio', False)
        self._monto = venta.get('monto', False)



    @staticmethod
    def browse(venta_id):
        browse_venta_query = """select * from venta where id = {venta_id}""".format(venta_id=venta_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_venta_query)
        venta = ps_cursor.fetchone()
        return venta

    @staticmethod
    def list_venta():
        browse_venta_query = """select * from venta"""
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_venta_query)
        venta = ps_cursor.fetchall()
        return venta

    @staticmethod
    def browsebyventa(estado):
        browse_venta_query = """select a.estado,b.name,c.name,a.cantidad,a.precio,a.monto from venta AS a INNER JOIN product as b ON (a.producto_id=b.id) inner join measure as c on (a.measure_id=c.id) where a.estado = {estado}""".format(estado=estado)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_venta_query)
        venta = ps_cursor.fetchall()
        return venta

    def create_venta(self):
        create_venta_query = """insert into venta(estado, producto_id,measure_id,cantidad, precio, monto) VALUES ('{estado}',{producto_id},{measure_id},'{cantidad}','{precio}','{monto}')""".format(estado=self.estado, producto_id=self.producto_id, measure_id=self.measure_id, cantidad=self.cantidad, precio=self.precio, monto=self.monto)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(create_venta_query)
        ps_connection.commit()
        ps_cursor.close()
        ps_connection.close()


    @property
    def estado(self):
        return self._estado

    @property
    def producto_id(self):
        return self._producto_id

    @property
    def measure_id(self):
        return self._measure_id

    @property
    def cantidad(self):
        return self._cantidad

    @property
    def precio(self):
        return self._precio

    @property
    def monto(self):
        return self._monto


    @estado.setter
    def estado(self, estado):
        self._estado = estado


    @producto_id.setter
    def producto_id(self, producto_id):
        self._producto_id = producto_id


    @measure_id.setter
    def measure_id(self, measure_id):
        self._measure_id = measure_id

    @cantidad.setter
    def cantidad(self, cantidad):
        self._cantidad = cantidad

    @precio.setter
    def precio(self, precio):
        self._precio = precio

    @monto.setter
    def monto(self, monto):
        self._monto = monto


    def __repr__(self):
        return "Venta" % self._name