  create table measure(
    id serial primary key,
    name character varying,
    code character varying
);

create table product(
    id serial primary key,
    name character varying,
    code character varying,
    measure_id integer,
    foreign key (measure_id) REFERENCES measure(id)
);
create table venta(
    id serial primary key,
    estado character varying,
    producto_id integer,
    measure_id integer,
    cantidad character varying,
    precio character varying,
    monto character varying,
    foreign key (producto_id) REFERENCES product(id),
    foreign key (measure_id) REFERENCES measure(id)
);


insert into measure(name, code) VALUES ('caja','m-001');
insert into measure(name, code) VALUES ('fantasias','m-002');
insert into product(name, code,measure_id) VALUES ('cafe','p-001',2);
insert into product(name, code,measure_id) VALUES ('te','p-002',2);
insert into product(name, code,measure_id) VALUES ('chocolate','p-003',2);
insert into product(name, code,measure_id) VALUES ('mantequilla','p-004',1);
insert into product(name, code,measure_id) VALUES ('coca cola','p-005',1);
insert into product(name, code,measure_id) VALUES ('agua','p-006',1);

insert into venta(estado, producto_id,measure_id,cantidad, precio, monto) VALUES ('vendido',1,1,'2','50','100');
insert into venta(estado, producto_id,measure_id,cantidad, precio, monto) VALUES ('vendido',4,2,'4','10','40');
insert into venta(estado, producto_id,measure_id,cantidad, precio, monto) VALUES ('agotado',4,2,'4','10','40');
insert into product(name, code,measure_id) VALUES ('agua','p-006',1);

--select * from product;
--select * from measure;
--drop table product;
--select * from venta ;

--delete from venta where id = 3;
SELECT a.estado,b.name,c.name,a.cantidad,a.precio,a.monto FROM venta AS a INNER JOIN product as b ON (a.producto_id=b.id) inner join measure as c on (a.measure_id=c.id) ;

--UPDATE measure SET name ='Unidad' WHERE id = 2;
--ALTER TABLE measure CHANGE fantasias unidad ;